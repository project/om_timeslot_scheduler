<?php

/**
* function to create CCK content types
*/
function _om_timeslot_scheduler_install_create_content_types() {

  // in order to create CCK content types, we use the form-based content_copy import/export functionality
  
  // create om_airing content type
  module_load_include('inc', 'om_timeslot_scheduler', 'includes/content_types/om_timeslot_theme_content_type');
  _om_timeslot_scheduler_install_create_om_timeslot_theme_content_type();

  // create om_feed content type
  module_load_include('inc', 'om_timeslot_scheduler', 'includes/content_types/om_timeslot_event_content_type');
  _om_timeslot_scheduler_install_create_om_timeslot_event_content_type();

  return;
} // function _om_timeslot_scheduler_install_create_content_types


/**
* function to import a new content type using CCK import functionality
*/
function _om_timeslot_scheduler_install_import_content_type($macro) {
  $form_state = array();
  $form = content_copy_import_form($form_state);
  $form_state['values']['type_name'] = '<create>';
  $form_state['values']['macro'] = $macro;
  return content_copy_import_form_submit($form, $form_state);
} // function _om_project_install_import_content_type  

